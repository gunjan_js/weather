# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repo contains a small project to get weather report of top cities of eurpoe. You can also drill down to see weather forecast for next 5 days of any of the cities.

### How do I get set up? ###

* This project doesn't need any backend. It is using open source weather data from openweathermap.org
* Download the code somewhere in your system.
* From command prompt install http-server `npm install -g http-server`
* Go to the location from command prompt and apply command `http-server -p 4321`
* Go to the browser and hit http://localhost:4321
* Congratulations, You started getting your weather report.

### Tools and Technologies

* jQuery
* underscore
* Backbone
* Marionette