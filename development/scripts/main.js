require.config({
    paths: {
        "jquery": "libraries/jquery/dist/jquery.min",
        "underscore": "libraries/underscore/underscore",
        "backbone": "libraries/backbone-amd/backbone-min",
        'backbone.babysitter': 'libraries/backbone.babysitter/lib/backbone.babysitter',
        'backbone.wreqr': 'libraries/backbone.wreqr/lib/backbone.wreqr',
        'backbone.radio': 'libraries/backbone.radio/build/backbone.radio',
        'marionette': 'libraries/marionette/lib/backbone.marionette',
        'materialize': 'libraries/materialize/dist/js/materialize.min'
    },
    shim: {
        "backbone": {
            deps: ["underscore", "jquery"],
            exports: "Backbone"
        },
        "marionette": {
            deps: ['backbone'],
            exports: 'Marionette'
        },

        "materialize": {
            deps: ["jquery", 'backbone', 'marionette'],
            exports: "materialize"
        }
    }
});

require(['application', 'router'], function (App, Router) {

    (function () {
        var app = new App();
        new Router();
        app.start();
    })();
});