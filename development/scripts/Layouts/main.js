define([
    'jquery',
    'underscore',
    'backbone',
    'marionette',
    'eventAggregator',
    'modules/cityList/collectionView',
    'modules/cityWeather/compositeView',
    'modules/cityList/collection',
    'modules/cityWeather/model',
    'modules/cityWeather/collection',
    'helper/credentials',
    'helper/cityList',
    'tpl!layouts/main.tmpl'
], function ($, _, Bcakbone, Marionette, EA, CityListView, CityWeatherView, CityListCollection, CityWeatherModel, CityWeatherCollection, CREDENTIALS, CITIES, template) {
     return Marionette.View.extend({
         template   :  template,
         className  :  'container',
         regions:{
             "mainRegion":"#mainRegion"
         },
         initialize: function(){
             var that = this,
             appid = CREDENTIALS.appid

             EA.on('cityList',function(){

                 var cityCollection = new CityListCollection();

                 cityCollection.fetch({

                     data:{
                         "id":CITIES.id.toString(),
                         "appid":appid,
                         "units":"metric"
                     },

                     success:function(response) {

                         var cityListView = new CityListView({
                             collection: response
                         });

                         that.showChildView('mainRegion', cityListView);
                     }
                 });
             });
             EA.on('cityWeather',function(city){

                 var cityWeather = new CityWeatherModel();

                 cityWeather.fetch({
                     data:{
                         "appid":appid,
                         "q":city
                     },
                     success:function(response) {
                         var cityCollection = new CityWeatherCollection(response.get('list'));

                         var cityWeatherView = new CityWeatherView({
                             collection: cityCollection,
                             model: cityWeather,
                         });

                         that.showChildView('mainRegion', cityWeatherView);
                    }

                 });
             })
         }
     });
});