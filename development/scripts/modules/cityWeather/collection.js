define([
    'jquery',
    'underscore',
    'backbone'
], function ($, _, Bcakbone) {
    return Backbone.Collection.extend({
        
        parse:function(data){

            return _.filter(data,function(obj,index){
                return new Date(obj.dt_txt).getHours() == 12
            });

        }

    });
});
