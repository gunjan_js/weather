define([
    'jquery',
    'underscore',
    'backbone',
    'marionette',
    'tpl!modules/cityWeather/item.tmpl'
],function($, _, Backbone, Marionette, template){
    return Marionette.View.extend({
        tagName: 'li',
        className: 'collection-item',
        template: template,
        events:{
            'click':'handleClick'
        },
        templateContext:{
            getDate:function(){
                var date = new Date(this.dt_txt);
                return date.getDate() + " " + this.getMonth(date.getMonth())
            },

            getMonth: function(index){
                var monthNames = ["Jan", "Feb", "March", "April", "May", "June",
                    "July", "Aug", "Sept", "Oct", "Nov", "Dec"
                ];
                return monthNames[index];
            },
            getDay: function(index){
                var date = new Date(this.dt_txt);
                var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
                return days[date.getDay()];
            }
        },
        handleClick:function(){
           // Backbone.history.navigate('/weather/'+this.model.get('name'))
        },

    })
});