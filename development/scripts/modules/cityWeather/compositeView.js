define([
    'jquery',
    'underscore',
    'backbone',
    'marionette',
    'modules/cityWeather/itemView',
    'tpl!modules/cityWeather/template.tmpl'
],function($, _, Backbone, Marionette, child, template){
     return Marionette.CompositeView.extend({
         childViewContainer:'ul',
         childView: child,
         template: template,
         events:{
             'click #backBtn': 'handleBackClick'
         },
         filter: function (child, index, collection) {
             return new Date(child.get('dt_txt')).getHours() == 12
         },
         handleBackClick:function(){
             Backbone.history.navigate('/weather', {
                 trigger: true
             })
         }
     })
});