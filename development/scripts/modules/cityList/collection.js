define([
    'jquery',
    'underscore',
    'backbone'
], function ($, _, Bcakbone) {
    return Backbone.Collection.extend({

        url:"http://api.openweathermap.org/data/2.5/group?&",

        parse:function(data){

            return data['list'];
        }
    });
});
