define([
    'jquery',
    'underscore',
    'backbone',
    'marionette',
    'modules/cityList/itemView'
],function($, _, Backbone, Marionette, child){
     return Marionette.CollectionView.extend({
         tagName: 'ul',
         className: 'collection',
         childView: child
     })
});