define([
    'jquery',
    'underscore',
    'backbone',
    'marionette',
    'tpl!modules/cityList/item.tmpl'
],function($, _, Backbone, Marionette, template){

    return Marionette.View.extend({

        tagName: 'li',

        className: 'collection-item',

        template: template,

        events:{
            'click':'handleClick'
        },

        handleClick:function(){

            Backbone.history.navigate('/weather/'+this.model.get('name'), {
                trigger: true
            })

        }
    })
});
