define([
    'jquery',
    'underscore',
    'backbone',
    'eventAggregator'
], function ($, _, Backbone, EA) {

    return Backbone.Router.extend({
        routes: {
            'weather': 'cityList',
            'weather/:id': 'cityWeather',
            "":"default"
        },
        cityList:function(){
            EA.trigger('cityList');
        },
        cityWeather:function(city){
            EA.trigger('cityWeather',city);
        },
        default:function(){
            Backbone.history.navigate('weather', {
                trigger: true
            })
        }
    });

});