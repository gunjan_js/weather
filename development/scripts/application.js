define([
    'jquery',
    'underscore',
    'backbone',
    'marionette',
    'layouts/main',
],function($, _, Backbone, Marionette, RootView){
    return Marionette.Application.extend({
        region: '#main',

        onStart: function() {
            this.showView(new RootView());
            if (Backbone.history) {
                Backbone.history.start();
            }
        }
    })
});