/*
 * @Author: gunjankothari
 * @Date:   21/06/17 11:18 PM
 */

define(['underscore', 'backbone'], function (_, Backbone) {
    return _.extend({}, Backbone.Events);
});